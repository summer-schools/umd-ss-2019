# Discrete Choice Modeling Summer School — 2019

This repository (repo) contains the material used throughout the Discrete Choice Modeling Summer School organized from May 20 to May 24, 2019, by the Transportation Engineering Department of the University of Maryland, College Park. The material is separated into folders by day of the school, and then by lecture and lab sessions conducted during the day. This README file contains information that a participant might find useful during and after the course of the summer school. Please feel free to email the organizers for requests, assistance or comments. The contact details of the organizers follow.

  - Cinzia Cirillo: [ccirillo@umd.edu](mailto:ccirillo@umd.edu "Send email to Cinzia Cirillo")
  - Javier Bas: [jbas@umd.edu](mailto:jbas@umd.edu "Send email to Javier Bas")
  - Lavan Teja Burra: [ltburra@umd.edu](mailto:ltburra@umd.edu "Send email to Lavan Teja Burra")
  - Riccardo Nali: [rnali@umd.edu](mailto:rnali@umd.edu "Send email to Riccardo Nali")
  - Kartik Kaushik: [kartikk@umd.edu](mailto:kartikk@umd.edu "Send email to Kartik Kaushik")

Please clone or download this repo to save a local copy on your computer. You may also repeat, execute and experiment with the included labs and data, and review the lecture slides at your convenience. However, the correct programing environment with all the required R and Python packages are required to successfully execute the lab scripts. The next section of this README provides the instructions to install the environments. Kindly contact Kartik Kaushik at any time during the summer school to request assistance and tech support for installing the environment, if required.

## Environment Installation

### Programming Languages
Python and R are the two main programming languages used for the labs in this summer school. The two programming languages, and associated required packages, are all open-source, and do not bear licensing costs. Administrator or root access is not required to install the software using the steps outlined here. It is urged of the participants to install the software on their regular use computers, and follow along with the lab instructions. Access to desktop computers (running Windows 10) with the required environments pre-installed will also be provided to participants of the summer school.


### Package and Environment Manager: `conda`
We will use the universal, open-source, cross-platform, language-agnostic package and environment manager called [`conda`](https://docs.conda.io/en/latest/ "About `conda`"). `conda` is released by Continuum Analytics and is available under the BSD license. The most famous flavor of `conda` is the [`Anaconda`](https://www.anaconda.com/ "Anaconda Homepage") distribution. However, `Anaconda` contains a lot of packages that will not be used during the summer school, and would just consume space on the users computer hard-drive. Consequently, we prefer the [`Miniconda`](https://docs.conda.io/en/latest/miniconda.html "Miniconda Homepage") flavor, which just installs the `conda` package and environment manager, and required dependencies.

### Step 1: Install `Miniconda`
[`Miniconda`](https://docs.conda.io/en/latest/miniconda.html "Miniconda Homepage") can be found either by clicking on the name at the start of this statement, or by navigating to [https://docs.conda.io/en/latest/miniconda.html](https://docs.conda.io/en/latest/miniconda.html "Miniconda Homepage"), or by searching for the term on an Internet search engine of choice. `Miniconda` running on 64-bit version of Python 3.7 and up is required for the labs in this summer school. Please follow the [installation instructions](https://conda.io/projects/conda/en/latest/user-guide/install/index.html) to install the latest copy of `Miniconda` for your computer.

### Step 2: Create the Environment
This and following steps require the use of a terminal. It is recommended to change the prompt directory to the parent directory of the summer school.
  - On **Windows**, a special prompt called `Anaconda Prompt` should be available in the Start-Menu after successful installation of `Miniconda`.
  - On **MacOS** and **Linux**, any regular terminal (or shell) can be used to install the environment (please check with `which conda` to ensure `conda` is available in the path variable).

Once the command `conda` is available at the prompt, please execute the following command to create an environment called `SumSch`, and install all required packages within.

```
conda create -n SumSch -c conda-forge python=3.7 R=3.5 autopep8 blas blosc bokeh bottleneck cython dask git h5py jupyter matplotlib numexpr numpy openpyxl pandas psycopg2 pytables python-dateutil pytz rpy2 scipy seaborn sqlalchemy statsmodels unidecode xarray xlsxwriter yaml r-boot r-copula r-data.table r-devtools r-digest r-dplyr r-essentials r-evaluate r-evd r-forecast r-formatr r-ggplot2 r-irkernel r-knitr r-matrix r-mvtnorm r-pryr r-snow r-tidyverse rstudio
```

### Step 3: Activate the Environment
After successfully creating the environment, activate it by entering the following command in the terminal.

```
conda activate SumSch
```

If the environment is loaded correctly, the prompt will be prefixed with `(SumSch)`. **The following steps require the environment to be activated successfully.**

### Step 4: Install Biogeme
Biogeme is available for download on the Python Package Index (PyPI). Precompiled `wheel` files of Biogeme for Windows 10, and MacOS running Python 3.7 are available, however, Linux or other configurations may require compiling Biogeme from source. Nevertheless, the command to install Biogeme is as follows. Please ensure the environment is loaded correctly, as it is designed to minimize issues when installing Biogeme.

```
pip install biogeme
```

### Step 5: Install the `dcmodels` R package
The last step is to install the `dcmodels` package created by Dr. Cirillo's research group. The package is available as a tarball called `dcmodels_1.0.01.tar.gz` in the `Packages` folder in this repo. Please note the full absolute path of the file for this step.

  - On **Windows**, absolute paths starts with a drive letter such as `C:`:
  ```
  R CMD INSTALL A:\absolute\path\to\dcmodels_1.0.01.tar.gz
  ```
  - On **MacOS** or **Linux**, absolute paths start with the root `/`:
  ```
  R CMD INSTALL /Absolute/path/to/dcmodels_1.0.01.tar.gz
  ```

## Post Installation Notes
Once the environment, Biogeme and the R package are installed correctly, the package is ready for use.

### Starting Jupyter Notebook
Most of the labs use a Jupyter notebook for executing the code interactively. With the `SumSch` environment loaded at the prompt, the following command will start a local Jupyter notebook server that can load and manage Python and R kernels.

```
jupyter notebook
```

The command should open the default browser at the directory of the prompt. If the prompt is based in the directory of the summer school, all sub-folders of the summer school will be available in the browser. It is recommended to change the directory using `cd` to the local directory of the summer school. Any of the `.ipynb` files in the summer school directory can be loaded and interacted with in the browser. Additionally, new Python or R notebooks can be launched. We refer to [The Jupyter Notebook Documentation](https://jupyter-notebook.readthedocs.io/en/stable/ "The Jupyter Notebook Documentation") (available at [https://jupyter-notebook.readthedocs.io/en/stable/](https://jupyter-notebook.readthedocs.io/en/stable/)) for a comprehensive Jupyter Notebook usage guide.

### Running Rstudio
The environment `SumSch` installs a copy of Rstudio that works using the R and associated packages in the environment. The copy will be called `Rstudio Desktop (SumSch)`. It is strongly advised to use that copy of Rstudio, to ensure the custom package `dcmodels` and other packages work cooperatively with each other. R code can also be executed in Jupyter Notebooks.

### Using git
The installed `SumSch` environment contains a copy of git. When the environment is active in a terminal, it can be used to clone (download) or pull changes from the git repo used for the summer school. The command is simple:

- If a local copy of the repo does **not exist**:

```
git clone https://gitlab.com/summer-schools/umd-ss-2019.git "Path/to/desired/local/folder/"
```

- If a local copy of the repo **exists**: Change the directory in the terminal to the directory of the repo, then run

```
git fetch origin master
git pull origin master
```

### Optionally installing Atom
[Atom](https://atom.io/) is a very powerful open-source text editor that can be endlessly customized to create a full-fledged general-purpose IDE, that can not only be used to write code, but run code interactively, and compile documents and manuscripts using Markdown or LaTeX. We recommend using the `Hydrogen`, `Rbox`, and other packages to improve the functionality of Atom. While we encourage self-motivated exploration of Atom, this [blog post](https://jstaf.github.io/2018/03/25/atom-ide.html) is a good starting point.

## Package Documentations

The package documentations can be found either by searching for the name of the package in a browser, or by clicking on the following links.

- [pandas](https://pandas.pydata.org/pandas-docs/stable/): https://pandas.pydata.org/pandas-docs/stable/
- [biogeme](https://biogeme.epfl.ch/documents.html): https://biogeme.epfl.ch/documents.html
- [numpy](https://docs.scipy.org/doc/numpy/user/index.html): https://docs.scipy.org/doc/numpy/user/index.html
- [git](https://guides.github.com/introduction/git-handbook/): https://guides.github.com/introduction/git-handbook/
